require 'pickup/version'
require 'json'

# rubocop:disable Style/Documentation
module Pickup
  # Raises a RuntimeError with a message if the argument being passed is nil.
  #
  # Parses the JSON received, iterates over each element assigning an empty
  # array as memo, fills it with a mapped array containing the values of the
  # token key everytime the second/last element in each array being accessed
  # is also an array.
  # Stores the result of the operation in a variable which is then used with
  # the normalize_tokens private method.
  # Returns an array of integer tokens.
  #
  # Example:
  #  collect_tokens JsonData::SCENARIO2
  # =>
  #  [790952, 103678, 788138, 802358]
  def self.collect_tokens(string)
    unless string.is_a? String
      raise "Expected to receive a String. #{string.class} object received."
    end

    tokens = JSON.parse(string).each_with_object([]) do |array, memo|
      last = array.last
      memo << last.map { |hash| hash.dig 'token' } if last.is_a? Array
    end

    normalize_tokens tokens
  end

  # Receives an array of arrays, iterates over each element inside each array,
  # stores them in a distributed way in a new array, and returns it.
  #
  # It creates an array with the same length as the highest element amongst all
  # the arrays in the array of arrays, pushes the sequence starting for the
  # first element in the first array until reaching the last one in it and
  # continues this way until the last element:
  #
  # Example:
  #   tokens = [
  #     %w[790952 802358 788138],
  #     %w[103678 562873 385093 803992],
  #     %w[788138 769163 789357]
  #   ]
  #   normalize_tokens tokens
  # =>
  #   array[0]: ["790952"]
  #   array[1]: ["802358"]
  #   array[2]: ["788138"]
  #   array[0]: ["790952", "103678"]
  #   array[1]: ["802358", "562873"]
  #   array[2]: ["788138", "385093"]
  #   array[3]: ["803992"]
  #   array[0]: ["790952", "103678", "788138"]
  #   array[1]: ["802358", "562873", "769163"]
  #   array[2]: ["788138", "385093", "789357"]
  def self.normalize_tokens(array_of_arrays)
    tokens = array_of_arrays.each_with_object([]) do |element, memo|
      element.each_with_index do |value, index|
        memo[index] ||= []
        memo[index] << value
      end
    end

    needed_amount_of_tokens tokens
  end

  # Receives an array of arrays, each of them containing String tokens, and
  # returns a new array with the needed amount, all of them as Integer values.
  #
  # It takes the array being passed and returns a new array where each array
  # inside has been extracted to the new one, giving a one-dimension flattened
  # array. Takes a range of elements from inside corresponding to the indexes
  # between 0 and 3. Map each element in the array converted to integer and
  # returns the new array.
  #
  # Example:
  #   tokens = [
  #     ["790952", "103678", "788138"],
  #     ["802358", "562873", "769163"],
  #     ["788138", "385093", "789357"],
  #     ["803992"]
  #   ]
  #   needed_amount_of_tokens tokens
  # => [790952, 103678, 788138, 802358]
  def self.needed_amount_of_tokens(array)
    array.flatten[0..3].map(&:to_i)
  end
end
