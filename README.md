# Pickup

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/pickup`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'pickup'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install pickup

## Usage

`Pickup.collect_tokens` expects to receive JSON data in a String data type object, in order to parse it and get the needed data from it:

```ruby
json_data = SCENARIO2
Pickup.collect_tokens json_data
# => [790952, 103678, 788138, 802358]
```
*[`SCENARIO2`](https://gist.github.com/luctus/3db17f4eb1a6d32eebaceb86afcc30e2#scenario-2)

In case the argument being used isn't a String, it returns a RuntimeError specifying the object type needed and the object type passed:

```ruby
Pickup.collect_tokens nil
# => "Expected to receive a String. NilClass object received."
Pickup.collect_tokens 1
# => "Expected to receive a String. Integer object received."
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/lagartoseb/pickup. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Pickup project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/lagartoseb/pickup/blob/master/CODE_OF_CONDUCT.md).
