require 'spec_helper'
require 'byebug'

# rubocop:disable Metrics/BlockLength
RSpec.describe Pickup do
  describe '.collect_tokens' do
    context 'with invalid argument' do
      let(:argument) { 1 }
      it 'raises a RuntimeError' do
        expect { Pickup.collect_tokens argument }.to raise_error(
          RuntimeError,
          "Expected to receive a String. #{argument.class} object received."
        )
      end
    end

    context 'with valid argument' do
      describe 'SCENARIO1' do
        it { expect(Pickup.collect_tokens(JsonData::SCENARIO1)).to eq [345_048] }
      end
      describe 'SCENARIO2' do
        subject { Pickup.collect_tokens(JsonData::SCENARIO2) }
        it { expect(subject).to eq [790_952, 103_678, 788_138, 802_358] }
      end
      describe 'SCENARIO3' do
        subject { Pickup.collect_tokens(JsonData::SCENARIO3) }
        it { expect(subject).to eq [103_678, 790_952, 802_358, 788_138] }
      end
      describe 'SCENARIO4' do
        subject { Pickup.collect_tokens(JsonData::SCENARIO4) }
        it { expect(subject).to eq [790_952, 103_678, 802_358, 562_873] }
      end
      describe 'SCENARIO5' do
        it { expect(Pickup.collect_tokens(JsonData::SCENARIO5)).to eq [] }
      end
      describe 'SCENARIO6' do
        it { expect(Pickup.collect_tokens(JsonData::SCENARIO6)).to eq [] }
      end
      describe 'SCENARIO7' do
        it { expect(Pickup.collect_tokens(JsonData::SCENARIO7)).to eq [] }
      end
      describe 'SCENARIO8' do
        subject { Pickup.collect_tokens(JsonData::SCENARIO8) }
        it { expect(subject).to eq [103_678, 790_952, 303_679, 802_358] }
      end
    end
  end
end
